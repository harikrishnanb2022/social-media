const route=require('express').Router();
const User=require('../models/User');
const bcrypt=require('bcrypt');

route.put("/:id",async(req,res)=>{
    if(req.body.userId===req.params.id || req.user.isAdmin){
        if(req.body.password){
            try{
                const salt=await bcrypt.genSalt(10);
                req.body.password=await bcrypt.hash(req.body.password,salt);     
            }catch(err){
                return res.status(500).json(err);
                
            }
        }
        try{
            const user=await User.findByIdAndUpdate(req.params.id,{
                $set:req.body,
            });
            res.status(200).json("Account updated")
        }catch(err){
            res.status(500).json(err);
        }
    }else{
        return res.status(403).json("Not your id")
    }
})

route.delete("/:id",async(req,res)=>{
    if(req.body.userId===req.params.id || req.body.isAdmin){
        try{
            const user=await User.findByIdAndDelete(req.params.id);
            res.status(200).json("Account deleted")
        }catch(err){
            res.status(500).json(err);
        }
    }else{
        return res.status(403).json("Not your id")
    }
});

route.get("/:id",async(req,res)=>{
    try{
        const user=await User.findById(req.params.id);
        const {password,updatedAt, ...other}=user._doc;
        res.status(200).json(other);
    }catch(err){
        res.status(500).json(err);
    }
})

route.put("/:id/follow", async(req,res)=>{
    if(req.body.userId !== req.params.id){
        try{
            const user=await User.findById(req.params.id);
            const currentUser=await User.findById(req.body.userId);
            if(!user.followers.includes(req.body.userId)){
                await User.updateOne({$push:{followers:req.body.userId}});
                await currentUser.updateOne({$push:{followings:req.params.id}});
                res.status(200).json("user has been followed");
            }
            else{
                res.status(403).json("you alraedy followed");
            }
        }catch(err){
            res.status(500).json("Error"+err);
        }
    }else{
        res.status(403).json("you cant follow yourself");
    }
});

route.put("/:id/unfollow", async(req,res)=>{
    if(req.body.userId !== req.params.id){
        try{
            const user=await User.findById(req.params.id);
            const currentUser=await User.findById(req.body.userId);
            if(user.followers.includes(req.body.userId)){
                await User.updateOne({$pull:{followers:req.body.userId}});
                await currentUser.updateOne({$pull:{followings:req.params.id}});
                res.status(200).json("user has been unfollowed");
            }
            else{
                res.status(403).json("you alraedy unfollowed");
            }
        }catch(err){
            res.status(500).json("Error"+err);
        }
    }else{
        res.status(403).json("you cant unfollow yourself");
    }
})
module.exports=route;