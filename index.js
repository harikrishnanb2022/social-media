const express =require('express')
const app= express();
const mongoose=require('mongoose')
const dotenv=require('dotenv')
const morgan=require('morgan')
const helmet=require('helmet');
const userRoute=require('./routes/users')
const authRoute=require('./routes/auth')
const postRoute=require('./routes/posts')
const mongo_URI="mongodb+srv://social_media:harikrish@cluster0.c6hlw.mongodb.net/socialmedia?retryWrites=true&w=majority";
mongoose.connect(mongo_URI,{useNewUrlParser:true,useFindAndModify:false,useUnifiedTopology:true,useCreateIndex:true},()=>{
    console.log('mongodb connected');
})

app.use(express.json());
app.use(helmet());
app.use(morgan("common"));
app.get('/',(req,res)=>{
    res.send("welcome all");
})
app.use('/user',userRoute);
app.use('/auth',authRoute);
app.use('/post',postRoute);
app.listen(5000,()=>{
    console.log('server is running')
})